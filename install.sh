#!/bin/bash

{

# Monitor
mkdir -p /etc/monitor /etc/monitor.d/available /etc/monitor.d/enabled
chown root /etc/monitor.d
chmod 755 /etc/monitor.d
[ -f /etc/monitor/monitor.sh ] || cp monitor.sh /etc/monitor
cp monitor-disable.sh /etc/monitor

# Cronjob
cp cronjob /etc/cron.d/monitor

# Logrotate
cat << ==end | sudo tee /etc/logrotate.d/monitor
/var/log/monitor.log {
    daily
    copytruncate
    rotate 30
    missingok
    notifempty
    dateext
}
==end

} >> /tmp/monitor-install.out
echo "$(tput setaf 2)Installed!$(tput sgr0)"
