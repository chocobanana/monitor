#!/bin/bash

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
MONITOR_D='/etc/monitor.d/enabled/'
MONITOR_LOG_FILE='/var/log/monitor.log'
MONITOR_TMP_FILE='/tmp/monitor.tmp'

logline() {
	echo "[`date '+%Y-%m-%d %H:%M:%S'`] [cronjob monitor] $1"
}

{

logline 'start'

#source /etc/monitor.d/ || { logline 'error'; exit 1; }

FILES=$(ls $MONITOR_D | sort -r )
for F in $FILES; do
    FILE=$MONITOR_D$F

    [ -f $FILE ] || continue
    
    # Clear vars
    PROCESS_NAME=''
    PROCESS_STATUS=''
    PROCESS_RESTART=''
    PROCESS_STDOUT=''

    # Include vars
    logline "Including file '$FILE'."
    source $FILE
    PROCESS_NAME=$F
    [ -z "$PROCESS_STATUS" ] && { logline "info: PROCESS_STATUS var is empty in file '$FILE'"; continue; }
    [ -z "$PROCESS_RESTART" ] && { logline "info: PROCESS_RESTART var is empty in file '$FILE'"; continue; }

    # Ensure process is running
    logline "Checking status for '$PROCESS_NAME'."
    eval "$PROCESS_STATUS" > $MONITOR_TMP_FILE || {
        logline "Process '$PROCESS_NAME' is not running. Restarting."
        if [ -z "$PROCESS_STDOUT" ]; then
            nohup $PROCESS_RESTART >> $MONITOR_LOG_FILE 2>&1 &
        else
            nohup $PROCESS_RESTART >> $PROCESS_STDOUT 2>&1 &
        fi
    }
done

logline 'end'
echo ''

} >> $MONITOR_LOG_FILE 2>&1

# EOF
