
# Monitor

## Install

- As end user

        $ cd /tmp
        $ wget https://bitbucket.org/chocobanana/monitor/get/master.zip
        $ unzip master.zip
        $ cd chocobanana-monitor<tab>
        $ sudo bash install.sh
        $ cd ..
        $ sudo mkdir -p /src/monitor
        $ sudo cp -r chocobanana-monitor<tab> /src/monitor/
    
- As developer of Monitor

        $ sudo su
        $ cd /srv/dev/public/
        $ git clone git@bitbucket.org:chocobanana/monitor.git monitor
        $ cd monitor
        $ bash install.sh
        $ ln -snf /srv/dev/public/monitor/monitor.sh /etc/monitor/monitor.sh

## Usage

### New process

Create a new file in the 'available' directory. Copy and paste example-1 or example-2 and modify.

    $ sudo vim /etc/monitor.d/available/myprocess

Then enable it by creating a symlink in the 'enabled' directory.

    $ ln -s /etc/monitor.d/available/myprocess /etc/monitor.d/enabled/myprocess

### Disable Monitor
    
    $ bash /etc/monitor/monitor-disable.sh

Note that this will only disable Monitor. The processes being monitored won't be stopped.

## End